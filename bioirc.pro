#-------------------------------------------------
#
# Project created by QtCreator 2013-12-09T17:47:04
#
#-------------------------------------------------

QMAKE_CXXFLAGS += -std=c++11

QT       += core network

QT       -= gui

LIBS += -lpcre -licuuc

TARGET = bioirc
TEMPLATE = lib

DEFINES += BIOIRC_LIBRARY

SOURCES += \
    bioirc/ircsocket.cpp \
    bioirc/ircparser.cpp

HEADERS +=\
    bioirc/ircsocket.h \
    bioirc/ircparser.h \
    bioirc/bioirc_global.h \
    bioirc/replycode.h
