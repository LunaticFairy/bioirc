#include "ircsocket.h"

using namespace bioirc;

IrcSocket::IrcSocket(QObject *parent = 0) {
    this->socket = new QTcpSocket(parent == 0 ? this : parent);
    this->connect(socket, &QTcpSocket::readyRead, this, &IrcSocket::readData);
    this->parser = IrcParser();
}

void IrcSocket::setNick(QString nickname) {
    this->userInfo.nickname = nickname;
}

void IrcSocket::setUserInfo(UserInfo user) {
    this->userInfo = user;
}

void IrcSocket::setNickserv(NickservInfo ns) {
    this->nsInfo = ns;
}

void IrcSocket::connectToServer(ConnInfo conn) {
    socket->connectToHost(conn.address, conn.port);
    this->writeRaw(QString("NICK %1").arg(this->userInfo.nickname));
    this->writeRaw(QString("USER %1 %2 * :%3").arg(this->userInfo.username).arg(this->userInfo.mode).arg(this->userInfo.realname));
    socket->flush();
}

void IrcSocket::disconnectFromServer(QString message) {
    this->writeRaw(QString("QUIT :%1").arg(message));
    socket->flush();
    if(socket->isOpen()) socket->close();
}

void IrcSocket::writeRaw(QString message) {
    socket->write(message.append("\r\n").toUtf8());
}

void IrcSocket::sendMessage(QString target, QString message) {
    this->writeRaw(QString("PRIVMSG %1 :%2").arg(target).arg(message));
}

void IrcSocket::sendNotice(QString target, QString message) {
    this->writeRaw(QString("NOTICE %1 :%2").arg(target).arg(message));
}

void IrcSocket::joinChannel(QString channel, QString key) {
    this->writeRaw(QString("JOIN %1 %2").arg(channel).arg(key.isNull() ? "" : key));
}

void IrcSocket::partChannel(QString channel, QString message) {
    this->writeRaw(QString("PART %1 :%2").arg(channel).arg(message.isNull() ? "" : message));
}

void IrcSocket::readData() {
    QString readLine = socket->readLine();
    readLine.chop(2);

    IRCData parsedData = this->parser.parseIRCData(readLine);
    if(!parsedData.isNull())
        emit onRawMessage(parsedData);
    if(!parsedData.command.isNull())
    {
        if(!parsedData.trail.isNull() && parsedData.command.toLower()=="ping")
            emit onServerPing(parsedData.trail);
        if(parsedData.command.toLower() == "privmsg" || parsedData.command.toLower() == "notice") {
            MessageData data;
            if(parsedData.prefix.contains("!")){
                data.nickname = parsedData.prefix.split("!").at(0);
                data.username = parsedData.prefix.split("!").at(1);
                data.hostname = parsedData.prefix.split("@").at(1);
            } else {
                data.hostname = parsedData.prefix;
            }
            data.target = parsedData.params;
            data.message = parsedData.trail;
            data.rawmessage = readLine;
            if(parsedData.command.toLower() == "privmsg")
                emit onMessageReceived(data);
            else
                emit onNoticeReceived(data);
        }
    }

    if(!parsedData.command.isNull() && parsedData.command.at(0).isDigit()) {
        try {
            int command = boost::lexical_cast<int>(parsedData.command.toStdString());
            emit onReplyCode(parsedData, command);
        } catch (boost::bad_lexical_cast) {
            std::cerr << "Bad lexical cast";
        }
    }

    if(socket->canReadLine()) readData();
}
