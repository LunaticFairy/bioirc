#ifndef IRCSOCKET_H
#define IRCSOCKET_H

#include "bioirc/bioirc_global.h"

#include <iostream>

#include <QObject>
#include <QByteArray>
#include <QtNetwork/QTcpSocket>
#include <boost/lexical_cast.hpp>

#include <bioirc/replycode.h>
#include <bioirc/ircparser.h>

namespace bioirc {
    struct ConnInfo {
        ConnInfo() {}
        ConnInfo(QString address_, int port_, QString server_password_ = QString::null) :
            address(address_), port(port_), server_password(server_password_) {}
        QString address;
        int port;
        QString server_password;
    };

    struct UserInfo {
        UserInfo() {}
        UserInfo(QString nickname, QString username, int mode, QString realname) :
            nickname(nickname), username(username), mode(mode), realname(realname) {}
        QString nickname;
        QString username;
        int mode;
        QString realname;
    };

    struct NickservInfo {
        NickservInfo() {}
        NickservInfo(QString username, QString password) :
            username(username), password(password) {}
        QString username;
        QString password;
    };

    struct MessageData {
        MessageData() {}
        MessageData(QString nick, QString user, QString host, QString targ, QString msg, QString raw) :
            nickname(nick), username(user), hostname(host), target(targ), message(msg), rawmessage(raw) {}
        QString nickname;
        QString username;
        QString hostname;
        QString target;
        QString message;
        QString rawmessage;
    };

    class SHARED_EXPORT IrcSocket: public QObject
    {
        Q_OBJECT

    public:
        explicit IrcSocket(QObject *parent);
        void setNick(QString nickname);
        void setUserInfo(UserInfo user);
        void setNickserv(NickservInfo ns);
        void connectToServer(ConnInfo conn);
        void disconnectFromServer(QString message);
        void writeRaw(QString message);
        void sendMessage(QString target, QString message);
        void sendNotice(QString target, QString message);
        void joinChannel(QString channel, QString key);
        void partChannel(QString channel, QString message);
    signals:
        void onRawMessage(IRCData data);
        void onServerPing(QString data);
        void onReplyCode(IRCData data, int code);
        void onMessageReceived(MessageData msg);
        void onNoticeReceived(MessageData msg);
    private:
        QTcpSocket * socket;
        IrcParser parser;

        ConnInfo connInfo;
        UserInfo userInfo;
        NickservInfo nsInfo;
    private slots:
        void readData();
    };
}


#endif // IRCSOCKET_H
