#ifndef IRCPARSER_H
#define IRCPARSER_H
#include "bioirc_global.h"
#include <pcre.h>
#include <QString>
#include <iostream>

#define MSG_LENGTH 300

namespace bioirc {
    //! IrcParsers data structure
    /*! A struct returned by IrcParser's parse() method */
    struct IRCData {
        IRCData() {}
        IRCData(QString prefix, QString command, QString params, QString trail) :
            prefix(prefix), command(command), params(params), trail(trail) {}
        void null() {
            this->prefix = QString::null;
            this->command = QString::null;
            this->params = QString::null;
            this->trail = QString::null;
        }
        bool isNull() {
            if(this->command.isNull() && this->params.isNull() && this->prefix.isNull() && this->trail.isNull()) return true;
            return false;
        }
        QString prefix;
        QString command;
        QString params;
        QString trail;
    };

    //! IRC Parser class
    /*! This class parses raw irc data and puts it into an IRCData struct */
    class SHARED_EXPORT IrcParser
    {
    public:
        //! Default constructor
        IrcParser();

        //! A function that parses irc data and returns an IRCData struct
        /*!
         \param input IRC data
         \return parsed IRC data
        */
        IRCData parseIRCData(QString input);
    };
}

#endif // IRCPARSER_H
