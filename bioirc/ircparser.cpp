#include "bioirc/ircparser.h"

using namespace bioirc;

IrcParser::IrcParser()
{

}

IRCData IrcParser::parseIRCData(QString input)
{
    IRCData data;
    data.null();

    try {
        const char *regex = "^(:(?<prefix>\\S+) )?(?<command>\\S+)( (?!:)(?<params>.+?))?( :(?<trail>.+))?$";
        int subStrVec[MSG_LENGTH];

        int erroffset = 0;
        const char *err;
        const char *pcreErrorStr;

        pcre *re = pcre_compile(regex, 0, &err, &erroffset, (char)PCRE_UTF8);
        if(re == NULL) {
            printf("ERROR: Could not compile '%s': %s\n", regex, pcreErrorStr);
            pcre_free(re);
            return data;
        }

        pcre_extra *pcreExtra = pcre_study(re, 0, &pcreErrorStr);
        if(pcreErrorStr != NULL) {
            printf("ERROR: Could not study '%s': %s\n", regex, pcreErrorStr);
            pcre_free(re);
            if(pcreExtra != NULL) pcre_free(pcreExtra);
            return data;
        }

        int pcreExecRet = pcre_exec(re, pcreExtra, input.toStdString().c_str(), strlen(input.toStdString().c_str()), 0, 0, subStrVec, 300);
        if(pcreExecRet < 0) {
              switch(pcreExecRet) {
              case PCRE_ERROR_NOMATCH      : printf("String did not match the pattern\n");        break;
              case PCRE_ERROR_NULL         : printf("Something was null\n");                      break;
              case PCRE_ERROR_BADOPTION    : printf("A bad option was passed\n");                 break;
              case PCRE_ERROR_BADMAGIC     : printf("Magic number bad (compiled re corrupt?)\n"); break;
              case PCRE_ERROR_UNKNOWN_NODE : printf("Something kooky in the compiled re\n");      break;
              case PCRE_ERROR_NOMEMORY     : printf("Ran out of memory\n");                       break;
              case PCRE_ERROR_BADUTF8      : printf("Bad UTF-8 data\n");                          break;
              default                      : printf("Unknown error\n");                           break;
              }
        } else {
            if(pcreExecRet == 0) {
                printf("But too many substrings were found to fit in subStrVec!\n");
                pcreExecRet = MSG_LENGTH / 4;
            }
            const char *psubPrefix = nullptr, *psubCommand = nullptr, *psubParams = nullptr, *psubTrail = nullptr;
            pcre_get_named_substring(re, input.toStdString().c_str(), subStrVec, pcreExecRet, "prefix", &(psubPrefix));
            pcre_get_named_substring(re, input.toStdString().c_str(), subStrVec, pcreExecRet, "command", &(psubCommand));
            pcre_get_named_substring(re, input.toStdString().c_str(), subStrVec, pcreExecRet, "params", &(psubParams));
            pcre_get_named_substring(re, input.toStdString().c_str(), subStrVec, pcreExecRet, "trail", &(psubTrail));

            data.prefix = (psubPrefix == nullptr) ? QString::null : QString::fromUtf8(psubPrefix, strlen(psubPrefix));
            data.command = (psubCommand == nullptr) ? QString::null : QString::fromUtf8(psubCommand, strlen(psubCommand));
            data.params = (psubParams == nullptr) ? QString::null : QString::fromUtf8(psubParams, strlen(psubParams));
            data.trail = (psubTrail == nullptr) ? QString::null : QString::fromUtf8(psubTrail, strlen(psubTrail));

            pcre_free_substring(psubPrefix);
            pcre_free_substring(psubCommand);
            pcre_free_substring(psubParams);
            pcre_free_substring(psubTrail);
        }

        pcre_free(re);
        if(pcreExtra != NULL) pcre_free_study(pcreExtra);
    } catch(...) {
        std::cerr << "Parser fault";
    }

    return data;
}
