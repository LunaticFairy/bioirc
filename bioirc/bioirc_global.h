#ifndef BIOIRC_GLOBAL_H
#define BIOIRC_GLOBAL_H

#include <QtCore/qglobal.h>

#if defined(BIOIRC_LIBRARY)
#  define SHARED_EXPORT Q_DECL_EXPORT
#else
#  define SHARED_EXPORT Q_DECL_IMPORT
#endif

#endif // BIOIRC_GLOBAL_H
